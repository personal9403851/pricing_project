const toggleButton = document.getElementById('toggle');
const basicPriceElement = document.getElementById('basic-price');
const professionalPriceElement = document.getElementById('professional-price');
const masterPriceElement = document.getElementById('master-price');

console.log(toggleButton);
console.log(basicPriceElement);

toggleButton.addEventListener('change', function () {
  const dollarSign = '<span class="dollar">$</span>';

  if (toggleButton.checked) {
    basicPriceElement.innerHTML = dollarSign + '199.99';
    professionalPriceElement.innerHTML = dollarSign + '249.99';
    masterPriceElement.innerHTML = dollarSign + '399.99';
  } else {
    basicPriceElement.innerHTML = dollarSign + '19.99';
    professionalPriceElement.innerHTML = dollarSign + '24.99';
    masterPriceElement.innerHTML = dollarSign + '39.99';
  }
});
